﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpScroller : MonoBehaviour, Fungus.IDialogInputListener
{
    [SerializeField] private UnityEngine.UI.ScrollRect _area;

    public void OnNextLineEvent() {
        ScrollUp();
    }

    public void ScrollUp() {
        _area.verticalNormalizedPosition = 1.0f;
    }

}
